
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "manu"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <p> Manufacturers </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-truck fa-fw"></i> Manufacturers</h1>
					<div class="section_functions">
						<a href="add-manufacturer.php" class="btn btn-primary"> + Add Manufacturer</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main undertab">
						<div class="search">
						</div>
						<table class="col-xs-12"> 
							<thead>
								<tr>
									<th>
										<a href="#">ID <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										<a href="#">Name <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										<a href="#">Address <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										Actions 
									</th>
							</thead>
							<tbody>
								<tr>
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr class="alt">
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr>
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr class="alt">
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr>
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr class="alt">
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr>
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr class="alt">
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr>
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr class="alt">
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr>
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr class="alt">
									<td> 241 </td>
									<td> Manufacturer Name </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>