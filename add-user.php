
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "users"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <a href="users.php">Users</a> > <p> Add User </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-tags fa-fw"></i> Add New User</h1>
					<div class="section_functions">
						<a href="stock.php" class="btn btn-primary" > Save User</a>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main">
						<div class="item-sidebar">
							<ul>
								<li class="active"><a href="#" data-tab="1">Basic </a></li> 
							</ul>
						</div>
						<div class="item-main">
							<div class="tab_content tabcontent_1 active" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Basic Information </h4>
										<div class="form-group">
											<label>First Name</label>
											<input type="text" name="Product Name" placeholder="Product Name" />
										</div>
										<div class="form-group">
											<label>Last Name</label>
											<input type="text" name="Product Name" placeholder="Product Name" />
										</div>
										<div class="form-group">
											<label>Username</label>
											<input type="text" name="Product Name" placeholder="Product Name" />
										</div>
										<div class="form-group">
											<label> Role</label>
											<select>
												<option>Select an option</option>
												<option>Admin </option>
												<option>User</option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
									</div>
								</form>
							</div>
							<div class="tab_content tabcontent_2" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Price Information </h4>
										<div class="form-group">
											<label>Price</label>
											<input type="text" name="Product Name" placeholder="Price" />
										</div>
										<div class="form-group">
											<label> VAT</label>
											<select>
												<option>Option #1 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<input type="text" name="Text Input" placeholder="Text Input" />
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<textarea  name="Description" placeholder="Text Area"></textarea>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
									</div>
								</form>
							</div>
							<div class="tab_content tabcontent_3" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Stock Information </h4>
										<div class="form-group">
											<label>Quantity</label>
											<input type="text" name="Product Name" placeholder="Quantity" />
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<input type="text" name="Text Input" placeholder="Text Input" />
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<textarea  name="Description" placeholder="Text Area"></textarea>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>