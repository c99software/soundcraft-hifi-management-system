
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "schedule"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <p> Schedule </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-calendar fa-fw"></i>Schedule</h1>
					<div class="section_functions">
						<a href="/schedule.php" class="btn btn-primary" > List View</a>
						<a href="#" class="btn btn-primary" > Calender View</a>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
						<div class="Filter">
							<div class="schedule_filter col-xs-12 col-sm-4">
								<label> Ticket Owner </label>
								<select>
									<option>All</option>
									<option>Geoff Mathews </option>
								</select>
							</div>
							<div class="schedule_filter col-xs-12 col-sm-4">
								<label> Date From </label>
								<input type="text" placeholder="Today">
							</div>
							<div class="schedule_filter col-xs-12 col-sm-4">
								<label> Date To</label>
								<input type="text" placeholder="All">
							</div>
						</div>
					<div class="col-xs-12 section_main">
						<?php 
							global $rowCount;
							$rowCount = '4';
							global $rowContent;
							$rowContent = array
							  (
							  array("Type 1","Volvo",'#Link1'),
							  array("Type 2","BMW",'#Link2'),
							  array("Type 3","Saab",'#Link3'),
							  array("Type 4","Land Rover",'#Link4'),
							  );
						?>
						<div id="schedule_list">
							
							<div class="col-xs-12 col-sm-12 item overdue">
								<div class="title ">
									<i class="fa fa-warning fa-fw"></i>
									<h4> Overdue</h4>
								</div>
								<div class="ticket type2">
									<a href="222" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-phone fa-fw"></i>
										<p> <b>Phone Customer: </b> </p>
										<p> Product they were interested in has come instock. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p> <b> Tags: </b> </p>
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
								<div class="ticket ">
									<a href="222" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-wrench fa-fw"></i>
										<p> <b>Installation: </b> </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p> <b> Tags: </b> </p>
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
							</div>
							<hr/>
							<div class="col-xs-12 col-sm-12 item">
								<div class="title">
									<h4>  <?php echo date("Y/m/d"); ?> </h4>
								</div>
								<div class="ticket type2">
									<a href="222" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-phone fa-fw"></i>
										<p> <b>Phone Customer: </b> </p>
										<p> Product they were interested in has come instock. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p> <b> Tags: </b> </p>
										<a href="#"> User: Geoff </a> 
										<span class="split">-</span>
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
								<div class="ticket ">
									<a href="222" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-wrench fa-fw"></i>
										<p> <b>Installation: </b> </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p> <b> Tags: </b> </p>
										<a href="#"> User: Geoff </a> 
										<span class="split">-</span>
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 item">
								<div class="title">
									<h4>  <?php echo date("Y/m/d",strtotime('+1 day')); ?> </h4>
								</div>
								<div class="ticket type1">
									<a href="222" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-pencil fa-fw"></i>
										<p> <b>Note: </b> </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p> <b> Tags: </b> </p>
										<a href="#"> User: Geoff </a> 
										<span class="split">-</span>
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
								<div class="ticket type1">
									<a href="222" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-pencil fa-fw"></i>
										<p> <b>General to do: </b> </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p> <b> Tags: </b> </p>
										<a href="#"> User: Geoff </a> 
										<span class="split">-</span>
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 item">
								<div class="title">
									<h4>  <?php echo date("Y/m/d",strtotime('+3 day')); ?> </h4>
								</div>
								<div class="ticket type3">
									<a href="222" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-phone fa-fw"></i>
										<p> <b>Phone Customer: </b> </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p> <b> Tags: </b> </p>
										<a href="#"> User: Geoff </a> 
										<span class="split">-</span>
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
								<div class="ticket ">
									<a href="222" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-wrench fa-fw"></i>
										<p> <b>Phone Customer: </b> </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p> <b> Tags: </b> </p>
										<a href="#"> User: Geoff </a> 
										<span class="split">-</span>
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>