
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "schedule"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <a href="schedule.php">Schedule</a> > <p> Note </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-tags fa-fw"></i> Schedule: Phone Customer</h1>
					<div class="section_functions">
						<a href="edit-product.php" class="btn btn-primary" > Edit Product</a>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main">
						<div class="item-sidebar">
							<ul>
								<li class="active"><a href="#" data-tab="1">Basic </a></li> 
							</ul>
						</div>
						<div class="item-main">
							<div class="tab_content tabcontent_1 active" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Basic Information </h4>
										<div class="form-group">
											<label>Title</label>
											<p> Phone Customer </p>
										</div>
										<div class="form-group">
											<label>Description</label>
											<p>  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consectetur adipiscing elit.  </p>
										</div>
										<div class="form-group">
											<label>Priority</label>
											<p> High </p>
										</div>
										<div class="form-group">
											<label>Created By </label>
											<p> User #2 </p>
										</div>
										<hr />
										<h4> Related: </h4>
										<div class="form-group">
											<label>Products:</label>
											<a  href="/view-product.php"> Product Title </a>
										</div>
										<div class="form-group">
											<label>Users:</label>
											<a href="/view-user.php"> User #2 </a>
										</div>
										<div class="form-group">
											<label>Customers</label>
											<a href="/view-customer.php"> Brian Thompson </a>
										</div>
								</form>
								
							</div>
							<div class="tab_content tabcontent_2" >	
								<div class="col-xs-12">
									

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>