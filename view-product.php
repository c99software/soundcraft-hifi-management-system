
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "products"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <a href="stock.php">Products</a> > <p> Product Title </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-tags fa-fw"></i> Product: Product Title</h1>
					<div class="section_functions">
						<a href="edit-product.php" class="btn btn-primary" > Edit Product</a>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main">
						<div class="item-sidebar">
							<ul>
								<li class="active"><a href="#" data-tab="1">Basic </a></li> 
								<li><a href="#" data-tab="2">Price </a></li> 
								<li><a href="#" data-tab="3">Stock </a></li> 
								<li><a href="#" data-tab="4">Notes </a></li> 
							</ul>
						</div>
						<div class="item-main">
							<div class="tab_content tabcontent_1 active" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Basic Information </h4>
										<div class="form-group">
											<label>Title</label>
											<p> Product Title </p>
										</div>
										<div class="form-group">
											<label> Description</label>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </p>
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </p>
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </p>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<p>Option #1</p>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<p>Option #2</p>
										</div>
									</div>
								</form>
							</div>
							<div class="tab_content tabcontent_2" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Price Information </h4>
										<div class="form-group">
											<label>Price</label>
											<p> &pound;292 </p>
										</div>
										<div class="form-group">
											<label> VAT</label>
											<p>Option #1 </p>
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </p>
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </p>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<p>Option #1</p>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<p>Option #2</p>
										</div>
									</div>
								</form>
							</div>
							<div class="tab_content tabcontent_3" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Stock Information </h4>
										<div class="form-group">
											<label>Quantity</label>
											<p>37</p>
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </p>
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </p>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<p>Option #1</p>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<p>Option #2</p>
										</div>
									</div>
								</form>
							</div>
							<div class="tab_content tabcontent_4" >
								<div class="col-xs-12">
									<div id="schedule_list" style="margin-top:15px;">
										<div class="col-xs-12 col-sm-12 item index">
											<div class="tickets">
												<div class="tickets">
													<div class="ticket type2">
														<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
															<i class="fa fa-phone fa-fw"></i>
															<p> <b>Phone Customer: </b> </p>
															<p> Product they were interested in has come instock. </p>
														</a>
														<a href="javascript:void(0)" class="expand"> + </a>
														<div class="tags col-xs-12 col-sm-12">
															<div class="tags-inner">
																<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
																<a href="/view-user.php"> User: Geoff </a> 
																<span class="split">-</span>
																 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
																<span class="split">-</span>
																<a href="/view-product.php"> Product: Example Project #2 </a>
															</div>
														</div>
													</div>
													<div class="ticket ">
														<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
															<i class="fa fa-wrench fa-fw"></i>
															<p> <b>Installation: </b> </p>
															<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
														</a>
														<a href="javascript:void(0)" class="expand"> + </a>
														<div class="tags col-xs-12 col-sm-12">
															<div class="tags-inner">
																<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
																 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
																<span class="split">-</span>
																<a href="/view-product.php"> Product: Example Project #2 </a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>