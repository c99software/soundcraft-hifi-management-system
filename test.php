
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "manu"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <p> Schedule </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-calendar fa-fw"></i>Schedule</h1>
					<div class="section_functions">
						<a href="edit-manufacturer.php" class="btn btn-primary" > Edit Manufacturer</a>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main">
						<?php 
							global $rowCount;
							$rowCount = '4';
							global $rowContent;
							$rowContent = array
							  (
							  array("Type 1","Volvo",'#Link1'),
							  array("Type 2","BMW",'#Link2'),
							  array("Type 3","Saab",'#Link3'),
							  array("Type 4","Land Rover",'#Link4'),
							  );
							include('/print_table.php');
						?>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>