
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "products"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <p> Products </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-tags fa-fw"></i> Products</h1>
					<div class="section_functions">
						<a href="add-product.php" class="btn btn-primary"> + Add Product</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main undertab">
						<div class="search">
						</div>
						<table class="col-xs-12"> 
							<thead>
								<tr>
									<th>
										<a href="#">ID <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										<a href="#">Title <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										<a href="#"> Price <i class="fa fa-caret-down  fa-fw"></i> </a>
									</th>
									<th>
										<a href="#">Description <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										<a href="#">Manufacturer <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										Actions 
									</th>
							</thead>
							<tbody>
								<tr>
									<td> 241 </td>
									<td> Product Title </td>
									<td> &pound;292.00 </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" >Manufacturer Name</a> </td>
									<td> <a href="view-product.php" class=""> View</a> | <a href="edit-product.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								<tr class="alt">
									<td> 241 </td>
									<td> Product Title </td>
									<td> &pound;292.00 </td>
									<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </td>
									<td> <a href="view-manufacturer.php" >Manufacturer Name</a> </td>
									<td> <a href="view-product.php" class=""> View</a> | <a href="edit-product.php" class=""> Edit</a> | <a href="#" class=""> Delete</a> </td>
								</tr>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>