
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "product"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <a href="stock.php">Products</a> > <p> Edit: Product Title </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-tags fa-fw"></i> Edit Product:</h1>
					<div class="section_functions">
						<a href="stock.php" class="btn btn-primary" > Save Product</a>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main">
						<div class="item-sidebar">
							<ul>
								<li class="active"><a href="#" data-tab="1">Basic </a></li> 
								<li><a href="#" data-tab="2">Price </a></li> 
								<li><a href="#" data-tab="3">Stock </a></li> 
							</ul>
						</div>
						<div class="item-main">
							<div class="tab_content tabcontent_1 active" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Basic Information </h4>
										<div class="form-group">
											<label>Title</label>
											<input type="text" name="Product Name" placeholder="Product Name" value="Product Title" />
										</div>
										<div class="form-group">
											<label> Description</label>
											<textarea  name="Description" placeholder="Description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas.</textarea>
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<input type="text" name="Text Input" placeholder="Text Input" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas."/>
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<textarea  name="Description" placeholder="Text Area">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas.</textarea>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option selected>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
												<select>
												<option>Select an option</option>
												<option selected>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
									</div>
								</form>
							</div>
							<div class="tab_content tabcontent_2" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Price Information </h4>
										<div class="form-group">
											<label>Price</label>
											<input type="text" name="Price" placeholder="Price" value="292" />
										</div>
										<div class="form-group">
											<label> VAT</label>
											<select>
												<option>Option #1 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<input type="text" name="Text Input" placeholder="Text Input" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas."/>
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<textarea  name="Description" placeholder="Text Area">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas.</textarea>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option selected>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
												<select>
												<option>Select an option</option>
												<option selected>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
									</div>
								</form>
							</div>
							<div class="tab_content tabcontent_3" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Stock Information </h4>
										<div class="form-group">
											<label>Quantity</label>
											<input type="text" name="Product Name" placeholder="Quantity" />
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<input type="text" name="Text Input" placeholder="Text Input" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas."/>
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<textarea  name="Description" placeholder="Text Area">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas.</textarea>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option selected>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
												<select>
												<option>Select an option</option>
												<option selected>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>