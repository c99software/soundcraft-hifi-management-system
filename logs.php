
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "users"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <p> Users </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-tags fa-fw"></i> Users: Activity Log</h1>
					<div class="section_functions">
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main undertab">
						<div class="search">
						</div>
						<table class="col-xs-12"> 
							<thead>
								<tr>
									<th>
										<a href="#">Entry ID <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										<a href="#">User <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
									<th>
										<a href="#"> Time <i class="fa fa-caret-down  fa-fw"></i> </a>
									</th>
									<th>
										<a href="#">Action <i class="fa fa-caret-down  fa-fw"></i></a>
									</th>
							</thead>
							<tbody>
								<tr>
									<td> 24 </td>
									<td> Product Title </td>
									<td> 11:41 <?php echo date("Y/m/d"); ?> </td>
									<td> <a href="/view-product.php"> Product Title</a>: Title changed </td>
								</tr>
								<tr>
									<td> 23 </td>
									<td> Product Title </td>
									<td> 11:31 <?php echo date("Y/m/d"); ?> </td>
									<td> <a href="/view-product.php"> Product Title</a>: Title changed </td>
								</tr>
								<tr>
									<td> 22 </td>
									<td> Product Title </td>
									<td> 10:21 <?php echo date("Y/m/d"); ?> </td>
									<td> <a href="/view-product.php"> Product Title</a>: Title changed </td>
								</tr>
								<tr>
									<td> 21 </td>
									<td> Product Title </td>
									<td> 9:11 <?php echo date("Y/m/d"); ?> </td>
									<td> <a href="/view-product.php"> Product Title</a>: Title changed </td>
								</tr>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>