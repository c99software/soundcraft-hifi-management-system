<?php 

/* Template: header */

$current_loc = trim($current_loc);
?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Soundcraft Hi Fi - Management System</title>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="sc_demo.css">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,500,500italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="Font-Awesome-master/css/font-awesome.min.css">
		<script src="js/jquery-1.11.3.min.js"></script>
		<script src="bootstrap/js/bootstrap.js"></script>
		<script src="js/custom.js"></script>
	</head>

	<body>
		<header>
			<div class="logo">
					<a href="index.php"><img src="img/logo2.png" title="Soundcraft HiFi" /></a>
			</div>
			<div class="nav-bottom">
				<h5> Logged in as: <span>Geoff Mathews</span> </h5>
			</div>
			<div class="search">
				<form>
					<input type="text" name="search" placeholder="Search...">
					<select> 
						<option>All</option>
						<option>Products</option>
						<option>Sales</option>
						<option>Customers</option>
						<option>Manufacturers</option>
						<option>Users</option>
						</select>
						<button> Search </button>
				</form>
			</div>
			<div id="main_navigation">
				<ul>
					<li class="<?php if($current_loc == 'dashboard') echo 'current';?>">
						<a href="index.php"><i class="fa fa-home fa-fw"></i> <span class="hidden-xs">Dashboard </span></span></a>
					</li>
					<li class="parent <?php if($current_loc == 'products') echo 'current';?>">
						<a href="#"><i class="fa fa-tags fa-fw"></i> <span class="hidden-xs">Products </span></a>
						<ul>
							<li><a href="stock.php"><i class="fa fa-tags fa-fw"></i> View All Products</a></li>
							<li><a href="add-product.php"><i class="fa fa-tags  fa-fw"></i> Add Product</a></li>
						</ul>
					</li>
					<li class="parent <?php if($current_loc == 'sales') echo 'current';?>">
						<a href="#"><i class="fa fa-shopping-basket fa-fw"></i> <span class="hidden-xs">Sales </span></a>
						<ul>
							<li><a href="#"><i class="fa fa-shopping-basket fa-fw"></i> Sales</a></li>
							<li><a href="#"><i class="fa fa-file-text  fa-fw"></i> Invoices </a></li>
							<li><a href="#"><i class="fa fa-file-text  fa-fw"></i> Quotes </a></li>
							<li><a href="#"><i class="fa fa-file-text  fa-fw"></i> Deposits</a></li>
						</ul>
					</li>
					<li class="<?php if($current_loc == 'schedule') echo 'current';?>">
						<a href="/schedule.php"><i class="fa fa-calendar fa-fw"></i> <span class="hidden-xs">Schedule</span></a>
					</li>
					<li class="parent <?php if($current_loc == 'customers') echo 'current';?>"><a href="#"><i class="fa fa-user fa-fw"></i> <span class="hidden-xs">Customers</span></a>
						<ul>
							<li><a href="customers.php"><i class="fa fa-user fa-fw"></i> View All Customers</a></li>
							<li><a href="#"><i class="fa fa-user  fa-fw"></i> Add Customer</a></li>
						</ul>
					</li>
					<li class="parent <?php if($current_loc == 'manu') echo 'current';?>"><a href="#"><i class="fa fa-truck fa-fw"></i> <span class="hidden-xs">Manufacturers </span></a>
						<ul>
							<li><a href="manufacturers.php"><i class="fa fa-truck fa-fw"></i> View All Manufacturers</a></li>
							<li><a href="add-manufacturer.php"><i class="fa fa-truck  fa-fw"></i> Add Manufacturer</a></li>
						</ul>
					</li>
					<li><a href="#"><i class="fa fa-bar-chart fa-fw"></i> <span class="hidden-xs">Reports</span> </a></li>
					<li class="parent <?php if($current_loc == 'users') echo 'current';?>"><a href="#"><i class="fa fa-key fa-fw"></i> <span class="hidden-xs">Users </span></a>
						<div class="nav-bottom">
						</div>
						<ul>
							<li><a href="/users.php"><i class="fa fa-key fa-fw"></i> View All Users</a></li>
							<li><a href="/logs.php"><i class="fa fa-key  fa-fw"></i> Activity Log</a></li>
							<li><a href="/add-user.php"><i class="fa fa-key  fa-fw"></i> Add User</a></li>
						</ul>
					</li>
					<li><a href="#"> <i class="fa fa-cog fa-fw"></i><span class="hidden-xs">My Account</span></a><li>
					<li><a href="#"> <i class="fa fa-bed fa-fw"></i><span class="hidden-xs">Logout</span></a><li>

				</ul>
			</div>
		</header>
		<div class="container-fluid main_content" id="dashboard">

		<?php 