
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = 'dashboard'; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="#">Home</a> > <p> Dashboard </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1> Welcome, <span> Geoff Mathews </span></h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main">
						<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. Vestibulum lacus felis, aliquam at pretium in, placerat eu lacus. Nulla cursus justo vitae est blandit luctus. Morbi nulla augue, fermentum at elementum quis, aliquet id sapien. </p>
						<div class="row">
							
							<div class="col-xs-12 col-sm-6 quick-links">
								<h2> Quick Links </h2>
								<div class=" col-xs-12 col-sm-6 col-md-4">
									<a href="/add-product.php" class=""><i class="fa fa-tags fa-fw"></i> Add <br/> Product </a>
								</div>
								<div class=" col-xs-12 col-sm-6 col-md-4">
									<a href="/add-manufacturer.php" class=""><i class="fa fa-truck fa-fw"></i> Add <br/> Manufacturer </a>
								</div>
								<div class=" col-xs-12 col-sm-6 col-md-4">
									<a href="/add-note.php" class=""><i class="fa fa-calendar fa-fw"></i> Add <br/> Note </a>
								</div>
								<div class="quick-note">
									<h2> Quick Add Note </h2>
									<div class="form-group col-xs-12 col-sm-6">
										<label> Title: </label>
										<input type="text"> 
									</div>
									<div class="form-group col-xs-12 col-sm-6">
										<label style="float:left;"> Priority: </label>
										<select>
											<option>Low</option>
											<option>Medium</option>
											<option>High</option>
										</select>
									</div>
									<div class="form-group col-xs-12 col-sm-12">
										<label style="float:left;"> Description: </label>
										<textarea type="text"> </textarea>
									</div>
									
									<div class="form-group col-xs-12 col-sm-12">
										<button class="btn btn-primary"> Add Note</button>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<h2> Todays Notes </h2>
								<div id="schedule_list" style="margin-top:15px;">
									<div class="col-xs-12 col-sm-12 item index">
										<div class="tickets">
											<div class="tickets">
												<div class="ticket type2">
													<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
														<i class="fa fa-phone fa-fw"></i>
														<p> <b>Phone Customer: </b> </p>
														<p> Product they were interested in has come instock. </p>
													</a>
													<a href="javascript:void(0)" class="expand"> + </a>
													<div class="tags col-xs-12 col-sm-12">
														<div class="tags-inner">
															<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
															<a href="/view-user.php"> User: Geoff </a> 
															<span class="split">-</span>
															 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
															<span class="split">-</span>
															<a href="/view-product.php"> Product: Example Project #2 </a>
														</div>
													</div>
												</div>
												<div class="ticket ">
													<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
														<i class="fa fa-wrench fa-fw"></i>
														<p> <b>Installation: </b> </p>
														<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
													</a>
													<a href="javascript:void(0)" class="expand"> + </a>
													<div class="tags col-xs-12 col-sm-12">
														<div class="tags-inner">
															<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
															 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
															<span class="split">-</span>
															<a href="/view-product.php"> Product: Example Project #2 </a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<h2> Recently Added </h2>
								<div id="schedule_list" style="margin-top:15px;">
									<div class="col-xs-12 col-sm-12 item index">
										<div class="tickets">
											<div class="ticket">
												<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
													<i class="fa fa-phone fa-fw"></i>
													<p> <b>Phone Customer: </b> </p>
													<p> Product they were interested in has come instock. </p>
												</a>
												<a href="javascript:void(0)" class="expand"> + </a>
												<div class="tags col-xs-12 col-sm-12">
													<div class="tags-inner">
														<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
														<a href="/view-user.php"> User: Geoff </a> 
														<span class="split">-</span>
														 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
														<span class="split">-</span>
														<a href="/view-product.php"> Product: Example Project #2 </a>
													</div>
												</div>
											</div>
											<div class="ticket ">
												<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
													<i class="fa fa-wrench fa-fw"></i>
													<p> <b>Installation: </b> </p>
													<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
												</a>
												<a href="javascript:void(0)" class="expand"> + </a>
												<div class="tags col-xs-12 col-sm-12">
													<div class="tags-inner">
														<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
														<a href="/view-user.php"> User: Geoff </a> 
														<span class="split">-</span>
														 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
														<span class="split">-</span>
														<a href="/view-product.php"> Product: Example Project #2 </a>
													</div>
												</div>
											</div>
											<div class="ticket ">
												<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
													<i class="fa fa-pencil fa-fw"></i>
													<p> <b>General to do: </b> </p>
													<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
												</a>
												<a href="javascript:void(0)" class="expand"> + </a>
												<div class="tags col-xs-12 col-sm-12">
													<div class="tags-inner">
														<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
														<a href="/view-user.php"> User: Geoff </a> 
														<span class="split">-</span>
														 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
														<span class="split">-</span>
														<a href="/view-product.php"> Product: Example Project #2 </a>
													</div>
												</div>
											</div>
											<div class="ticket ">
												<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
													<i class="fa fa-wrench fa-fw"></i>
													<p> <b>Installation: </b> </p>
													<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
												</a>
												<a href="javascript:void(0)" class="expand"> + </a>
												<div class="tags col-xs-12 col-sm-12">
													<div class="tags-inner">
														<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
														<a href="/view-user.php"> User: Geoff </a> 
														<span class="split">-</span>
														 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
														<span class="split">-</span>
														<a href="/view-product.php"> Product: Example Project #2 </a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<!-- Content End -->

	
<?php include 'footer.php'; ?>