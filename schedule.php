
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "schedule"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <p> Schedule </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-calendar fa-fw"></i>Schedule</h1>
					<div class="section_functions">
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					
					<div class="col-xs-12 section_main">
						<?php 
							global $rowCount;
							$rowCount = '4';
							global $rowContent;
							$rowContent = array
							  (
							  array("Type 1","Volvo",'#Link1'),
							  array("Type 2","BMW",'#Link2'),
							  array("Type 3","Saab",'#Link3'),
							  array("Type 4","Land Rover",'#Link4'),
							  );
						?>
						<div id="schedule_list">
							
							<!-- 
							<div class="col-xs-12 col-sm-12 item overdue">
								<div class="title ">
									<i class="fa fa-warning fa-fw"></i>
									<h4> Overdue</h4>
								</div>
								<div class="ticket type2">
									<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-phone fa-fw"></i>
										<p> <b>Phone Customer: </b> </p>
										<p> Product they were interested in has come instock. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
								<div class="ticket ">
									<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-6">
										<i class="fa fa-wrench fa-fw"></i>
										<p> <b>Installation: </b> </p>
										<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
									</a>
									<div class="tags col-xs-12 col-sm-6">
										<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
										 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
										<span class="split">-</span>
										<a href="/view-product.php"> Product: Example Project #2 </a>
									</div>
								</div>
							</div>
							<hr/>
							-->
							<div class="col-xs-12 col-sm-3 filter-secondary">
								<h3> Filter </h3>
								<div class="form-group">
									<i class="fa fa-pencil fa-fw"></i><label> General Notes </label> 
									<input type="checkbox" checked>	
								</div>
								<div class="form-group">
									<i class="fa fa-wrench fa-fw"></i><label> Installations </label> 
									<input type="checkbox" checked>
								</div>
								<div class="form-group">
									<i class="fa fa-phone fa-fw"></i><label> Contact Customer </label> 
									<input type="checkbox" checked>
								</div>
								<div class="form-group">
									<i class="fa fa-wrench fa-fw"></i><label> Installations </label> 
									<input type="checkbox" checked>
								</div>
								<hr />
								<div class="form-group">
									<span class="red-box"></span><label> High Priority </label> 
									<input type="checkbox" checked>
								</div>
								<div class="form-group">
									<span class="orange-box"></span><label> Medium Priority </label> 
									<input type="checkbox" checked>
								</div>
								<div class="form-group">
									<span class="green-box"></span><label> Low Priority </label> 
									<input type="checkbox" checked>
								</div>
								<hr />


								<div class="form-group">
									<label> Days Shown</label>
									<select>
										<option selected> 7</option>
										<option> 14</option>
										<option> 30</option>
									</select>
								</div>

								<div class="form-group">
									<label> Ticket Owner </label>
									<select>
										<option>All</option>
										<option>Geoff Mathews </option>
									</select>
								</div>
								<div class="form-group">
									<label> Date From </label>
									<input type="text" placeholder="Today">
								</div>
							</div>
							<div class="col-xs-12 col-sm-9 item">
									<h3> Showing <?php echo date("Y/m/d"); ?> - <?php echo date("Y/m/d",strtotime('+7 day')); ?></h3>
								<div class="title col-xs-2">
									<h4> <h4>  <?php echo date("Y/m/d",strtotime('+1 day')); ?> </h4> </h4>
								</div>
								<div class="tickets col-xs-10">
									<div class="ticket type2">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-phone fa-fw"></i>
											<p> <b>Phone Customer: </b> </p>
											<p> Product they were interested in has come instock. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-wrench fa-fw"></i>
											<p> <b>Installation: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
								</div>
								<hr class="alt" />
								<div class="title col-xs-2">
									<h4>  <?php echo date("Y/m/d"); ?> </h4>
								</div>
								<div class="tickets col-xs-10">
									<div class="ticket">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-phone fa-fw"></i>
											<p> <b>Phone Customer: </b> </p>
											<p> Product they were interested in has come instock. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket type3">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-wrench fa-fw"></i>
											<p> <b>Installation: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
								</div>

								<hr class="alt" />
								<div class="title col-xs-2">
									<h4>  <?php echo date("Y/m/d",strtotime('+2 day')); ?> </h4>
								</div>
								<div class="tickets col-xs-10">
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-phone fa-fw"></i>
											<p> <b>Phone Customer: </b> </p>
											<p> Product they were interested in has come instock. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket type2">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
								</div>
								<hr class="alt" />
								<div class="title col-xs-2">
									<h4>  <?php echo date("Y/m/d",strtotime('+6 day')); ?> </h4>
								</div>
								<div class="tickets col-xs-10">
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-phone fa-fw"></i>
											<p> <b>Phone Customer: </b> </p>
											<p> Product they were interested in has come instock. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket type2">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
								</div>
								<hr class="alt" />
								<div class="title col-xs-2">
									<h4>  <?php echo date("Y/m/d",strtotime('+7 day')); ?> </h4>
								</div>
								<div class="tickets col-xs-10">
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-phone fa-fw"></i>
											<p> <b>Phone Customer: </b> </p>
											<p> Product they were interested in has come instock. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket ">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
									<div class="ticket type2">
										<a href="/view-note.php" class="ticket-link col-xs-12 col-sm-12">
											<i class="fa fa-pencil fa-fw"></i>
											<p> <b>General to do: </b> </p>
											<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
										</a>
										<a href="javascript:void(0)" class="expand"> + </a>
										<div class="tags col-xs-12 col-sm-12">
											<div class="tags-inner">
												<p><b>Created By:</b></p><a href="/view-user.php"> User: Geoff </a> <hr/> <p> <b> Tags: </b> </p> 
												<a href="/view-user.php"> User: Geoff </a> 
												<span class="split">-</span>
												 <a href="/view-customer.php"> Customer: Brian Thompson </a> 
												<span class="split">-</span>
												<a href="/view-product.php"> Product: Example Project #2 </a>
											</div>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		

	</body>
</html>