(function($){
	$( document ).ready(function() {
		wdw_height = $(window).height();
		
	   	$('header').css('min-height',wdw_height);
	   	$('.main_content').css('min-height',wdw_height);
	   	sct_height = $('.form_section').height();
	   	$('.item-sidebar').css('min-height',sct_height+'px');

	   	$('#main_navigation ul li.parent a ').on('click',function(){
	   		$parent_li = $(this).closest('li');
	   		if($parent_li.hasClass('active')){
	   			$parent_li.removeClass('active');
	   		}
	   		else{
	   			$('#main_navigation ul li.active').removeClass('active');
	   			$parent_li.addClass('active');
	   		}
	   	});

	   	$('.item-sidebar ul li a ').on('click',function(){
	   		var tab = $(this).attr('data-tab');
	   		$parent_li = $(this).closest('li');
	   		if(!$parent_li.hasClass('active')){
	   			$('.item-sidebar ul li.active').removeClass('active');
	   			$parent_li.addClass('active');
	   			$('.tab_content.active').removeClass('active');
	   			$('.tab_content.tabcontent_'+tab).addClass('active');
	   			sct_height = $('.tab_content.tabcontent_'+tab+' .form_section').height();
	   			$('.item-sidebar').css('min-height',sct_height+'px');

	   		}
	   	});
	   	$('.expand').on('click',function(){
				if($(this).closest('.ticket').find('.tags').hasClass('active')){
					$(this).closest('.ticket').find('.tags').removeClass('active');
					$(this).text('+');
				}
				else
				{
					$(this).closest('.ticket').find('.tags').addClass('active');
					$(this).text('-');
				}
			});
	});
})(jQuery);