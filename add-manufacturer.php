
<!--
This is a skeleton html file that you can use to get you started on each new
HTML project

Name: Your Name Here
Class: CIS 3303
Section: x
-->
<?php global $current_loc; ?>
<?php $current_loc = "manu"; ?>
<?php include 'header.php'; ?>

<!-- Content Start -->
			<div class="row breadcrumbs">
				<div class="col-xs-12 ">
					<div class="col-xs-12 section_main">
						<a href="index.php">Home</a> > <a href="manufacturers.php">Manufacturers</a> > <p> Add manufacturer </p>
					</div>
				</div>
			</div>
			<div class="row section_header">
				<div class="col-xs-12">
					<h1><i class="fa fa-truck fa-fw"></i> Add New manufacturer</h1>
					<div class="section_functions">
						<a href="manufacturers.php" class="btn btn-primary" > Save manufacturer</a>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-12 section_main">
						<div class="item-sidebar">
							<ul>
								<li class="active"><a href="#" data-tab="1">Basic </a></li> 
								<li><a href="#" data-tab="3"> Manufacturer Products</a></li> 
							</ul>
						</div>
						<div class="item-main">
							<div class="tab_content tabcontent_1 active" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Basic Information </h4>
										<div class="form-group">
											<label>Title</label>
											<input type="text" name="manufacturer Name" placeholder="manufacturer Name" />
										</div>
										<div class="form-group">
											<label> Description</label>
											<textarea  name="Description" placeholder="Description"></textarea>
										</div>
										<div class="form-group">
											<label> Text Input</label>
											<input type="text" name="Text Input" placeholder="Text Input" />
										</div>
										<div class="form-group">
											<label> Text Area</label>
											<textarea  name="Description" placeholder="Text Area"></textarea>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
										<div class="form-group">
											<label> Dropdown</label>
											<select>
												<option>Select an option</option>
												<option>Option #1 </option>
												<option>Option #2 </option>
												<option>Option #3 </option>
												<option>Option #4 </option>
											</select>
										</div>
									</div>
								</form>
							</div>
							
							<div class="tab_content tabcontent_3" >
								<form> 
									<div class="form_section col-xs-12">
										<h4> Manufacturer Products</h4>
										<div class="form-group">
											<table>
												<thead>
													<tr>
														<th> <a href='#'>ID</a></th>
														<th><a href='#'>Title</a></th>
														<th> <a href='#'>Desc</a></th>
														<th> Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td> 251</th>
														<td> Product Title </th>
														<td> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ultricies tristique tellus sit amet egestas. </th>
														<td> <a href="view-manufacturer.php" class=""> View</a> | <a href="edit-manufacturer.php" class=""> Edit</a> </td>
													</tr>
												</tbody>
											</table>
										</div>
										
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>